package cn.nhs.schedule.controller.service;

import cn.nhs.schedule.domain.DataCollect;
import cn.nhs.schedule.domain.DcsScheduleInfo;
import cn.nhs.schedule.domain.DcsServerNode;
import cn.nhs.schedule.domain.Instruct;

import java.util.List;


public interface DcsScheduleService {

    List<String> queryPathRootServerList() throws Exception;

    List<DcsScheduleInfo> queryDcsScheduleInfoList(String schedulerServerId) throws Exception;

    void pushInstruct(Instruct instruct) throws Exception;

    DataCollect queryDataCollect() throws Exception;

    List<DcsServerNode> queryDcsServerNodeList() throws Exception;

}